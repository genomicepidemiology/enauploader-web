#!/usr/bin/perl

# By: Rolf Sommer Kaas
# Date: 11/2 - 2015
#
# Description:
# ENA upload script for COMPARE/CGE pipeline
#
# TODO: Make "files" hash obsolete!

BEGIN{push @INC, '/home/data1/tools/bin/perl_modules/lib/perl5';}
# use local::lib '/home/data1/tools/bin/perl_modules/lib/';

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Basename;

use Spreadsheet::XLSX;            # For reading Excel files
use Excel::Writer::XLSX;          # For writing Excel files
use Date::Calc qw(N_Delta_YMD Add_Delta_Days Delta_Days);
use XML::LibXML;
use Digest::MD5::File;
use Net::FTP;
use Bio::DB::Taxonomy;
#use Text::Ispell qw( spellcheck );
#Text::Ispell::allow_compounds(1);

#
# Load translation table for host names if it exists
#

my $host_transl_file = "/home/data1/services/ENAUploader/ENAUploader-2.1/scripts/host_transl.txt";
my %host_translation;
if(-s $host_transl_file){
	print STDERR ("Host translation table loaded: $host_transl_file\n");
	%host_translation= %{ &loadTranslTable($host_transl_file) };
}

#
# Get local time (This might need to be changed?)
#
# Get todays date in YYYY-MM-DD format (used later for calculating the difference between today and the release date)
# today array = [YYYY, MM, DD]
my @today = (gmtime())[5,4,3]; #stores element 3, 4 and 5 from the localtime output in an array
$today[0] += 1900;
$today[1] ++;
for(my $i=1; $i<3; $i++){
	my $number = $today[$i];
	if( $number < 10){
		$today[$i] = "0$number";
	}
}
my @time = (gmtime())[2,1,0];
for(my $i=0; $i<3; $i++){
	my $number = $time[$i];
	if( $number < 10){
		$time[$i] = "0$number";
	}
}

# DEBUG
print STDERR ("Today is: $today[0]-$today[1]-$today[2]\n");

#
# Initiate constants
#

# Name of sheet where metadata resides
use constant SHEET_DATA_NAME => "Metadata";

# Required fields for submission
use constant AVAILABILITY => "usage_restrictions";
use constant DATA_RELEASE => "release_date";
use constant SAMPLE_TYPE  => "sample_type";
use constant SEQ_METHOD   => "sequencing_platform";
use constant READ_DATA    => "file_names";
use constant SAMPLE_ID    => "sample_name"; # MUST be unique!
use constant SEQ_TYPE     => "sequencing_type";
#     Missing fields in pipeline
use constant INSERT_SIZE  => "insert_size";
# If we calc it ourselves, we can add NOMINAL_SDEV

my %required_headers = (
	AVAILABILITY() => 1,
	DATA_RELEASE() => 1,
	SAMPLE_TYPE()  => 1,
	SEQ_METHOD()   => 1,
	READ_DATA()    => 1,
	SAMPLE_ID()    => 1,
	SEQ_TYPE()     => 1
);

# Required fields for GMI:MDM
use constant HOST => "isolation_source"; # Different name in sheet?
use constant STRAIN_SUPPLIER => "collected_by";
use constant ISOLATION_DATE => "collection_date";
use constant COUNTRY => "country";
use constant STRAIN_NAME => "strain";
use constant ORGANISM => "organism";
#     Missing fields in pipeline
use constant PATHOGEN_TYPE => "Type of pathogen";
use constant HOST_DISEASE => "Host disease";
use constant ISOLATION_SOURCE => "Isolation source";

#use constant GENUS => "Genus";
#use constant SPECIES => "Species (serovar for Salmonella)";

my %mdm_req_headers = (
	ORGANISM()         => 1,
	HOST()             => 1,
	PATHOGEN_TYPE()    => 1,
	HOST_DISEASE()     => 1,
	STRAIN_SUPPLIER()  => 1,
	ISOLATION_DATE()   => 1,
	COUNTRY()          => 1,
	STRAIN_NAME()      => 1
);

# Optional fields
use constant LON       => "longitude"; # (WGS 84)
use constant LAT       => "latitude";  # (WGS 84)
use constant SEQ_MODEL => "instrument_model";

# constants array
my @constant_headers = (	SAMPLE_ID, STRAIN_NAME, ORGANISM, HOST, PATHOGEN_TYPE, LON, LAT,
							HOST_DISEASE, ISOLATION_SOURCE, STRAIN_SUPPLIER, SEQ_MODEL,
							ISOLATION_DATE, COUNTRY, AVAILABILITY, DATA_RELEASE,
							SAMPLE_TYPE, SEQ_METHOD, READ_DATA, INSERT_SIZE, SEQ_TYPE	);

# Initiate constants found in the GMI:MDM fields
use constant ENA_HOST_ASSOCIATED => "Is the sequenced pathogen host associated?";		#
use constant ENA_HOST => "host scientific name";										#
use constant ENA_HOST_STATUS => "host_status";											#
use constant ENA_ISOLATION_SOURCE_HOST => "isolation_source_host_associated";			#
use constant ENA_ISOLATION_SOURCE_NON_HOST => "isolation_source_non_host_associated";	#
use constant ENA_STRAIN_SUPPLIER => "collected_by";										#
use constant ENA_ISOLATION_DATE => "collection_date";									#
use constant ENA_LAT => "geographic location (latitude)";								#
use constant ENA_LON => "geographic location (longitude)";								#
use constant ENA_COUNTRY => "geographic location (country and/or sea)";					#
use constant ENA_ENV_SAMPLE => "environmental_sample";								### MISSING ###
use constant ENA_SAMPLE_ID => "isolate";												#
use constant GMI_MDM_TAG => "ENA-CHECKLIST";											#
use constant GMI_MDM_VAL => "ERC000029";												#  Min. acc. checklist ERC000028
# Missing from meta data sheet
use constant ENA_STRAIN => "strain";   													#
use constant ENA_SEROVAR => "serovar";												### MISSING ###

my %ena2meta = (
	ENA_HOST()                  => HOST,
	ENA_ISOLATION_SOURCE_HOST() => ISOLATION_SOURCE,
	ENA_STRAIN_SUPPLIER()       => STRAIN_SUPPLIER,
	ENA_ISOLATION_DATE()        => ISOLATION_DATE,
	ENA_LAT()                   => LAT,
	ENA_LON()                   => LON,
	ENA_COUNTRY()               => COUNTRY,
	ENA_SAMPLE_ID()             => SAMPLE_ID,
	ENA_STRAIN()                => STRAIN_NAME
);

#
# Handle input arguments
#

my $opt_input;
my $opt_output = "ENA_sub_output";
my $opt_test;
my $opt_data_folder;
my $opt_title;
my $opt_abstract;
my $opt_web = "";
my $opt_uploaded;
my $opt_validate;
my $opt_md5;
my $opt_excel;
my $opt_external;

# Static input
my $centre_ac = "DTU-GE";
my $opt_lib_strat = "WGS";
my $opt_lib_select = "RANDOM";
my $opt_auth;

&GetOptions (
	"i|input=s"         => \$opt_input,       # File with meta data, expected format is excel
	"excel"             => \$opt_excel,       # Meta data file is in excel format
	"o|output_prefix=s" => \$opt_output,      # create files: output.log output.xlsx
	"t|test"            => \$opt_test,        # test upload, not real!
	"d|data_folder=s"   => \$opt_data_folder, # Will look for the files in this folder
	"title=s"           => \$opt_title,       # Title (string) of submitted study
	"abstract=s"        => \$opt_abstract,    # Path to raw text file containing the abstract for the study.
	"uploaded"          => \$opt_uploaded,    # Do not upload / (assume files are uploaded)
	"validate"          => \$opt_validate,    # Do not ADD but VALIDATE submission instead
    "auth=s"            => \$opt_auth,        # File containing username (line 1) and password (line2).

	"md5=s"             => \$opt_md5,         #
	"external"          => \$opt_external,
	"w|web=s"           => \$opt_web          # Due to changes this might just be changed to output_prefix?
);

# If external, sets a prefix to all read file paths.
if($opt_external){
	$opt_external = "Uploads";
}

# If MD5 sums is given, they don't need to unless files has already been uploaded.
my %md5_sums;
if($opt_md5){
	open(MDSUMS, "<$opt_md5") or die("Couldn't open md5 sums file: $opt_md5\n");
	while(<MDSUMS>){
		chomp;
		next unless ($_);
		my ($sum, $file) = split(/\s+/);
		$md5_sums{$file} = $sum;
	}
	close(MDSUMS);
}

# Output files
my $out_log = "$opt_output.log";
my $out_upload_log = "$opt_output.upload.log";
my $out_excel = "$opt_output.xlsx";
if($opt_web){
	$out_log = "downloads/".$out_log;
	$out_upload_log = "downloads/".$out_upload_log;
	$out_excel = "downloads/".$out_excel;
}

# Create url for submitting XML files to ENA.
my $url;
my $hostname = "webin.ebi.ac.uk";
open(AUTH, "<$opt_auth") or die("Couldn't open auth file: $opt_auth");
my $username = <AUTH>;
chomp($username);
my $password = <AUTH>;
chomp($password);
close(AUTH);

if($opt_test){
	$url = "https://www-test.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20$username%20$password";
}
else{
	$url = "https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20$username%20$password";
}

# Title and Abstract
my $abstract;
open(ABSTRACT, "<$opt_abstract") or die("Couldn't open abstract file: $opt_abstract\n");
while(<ABSTRACT>){
	# Replacing windows newlines.
	$_ =~ s/\r\n/\n/g;
	# Replacing mac newlines.
	$_ =~ s/\r/\n/g;
	$abstract .= $_;
}
close(ABSTRACT);

my $log = "";

#
# Read metadata
#
my $excel;

my %header;
my @headers;
my $sheet;

my @input_meta; # array of arrays [row][column]

my $min_col = 0;
my $max_col;
my $min_row = 1;
my $max_row;

# Excel sheet
if($opt_excel){
	$excel = Spreadsheet::XLSX -> new ($opt_input);

	# Extracts Excel sheet labeled as constant SHEET_DATA_NAME.
	foreach my $current_sheet (@{$excel -> {Worksheet}}) {
		if($current_sheet->{Name} eq SHEET_DATA_NAME){
			$sheet = $current_sheet;
			last;
		}
	}
	unless( defined($sheet) ){
		$log .= "The Excel file given as input did not contain the expected sheet named \"".SHEET_DATA_NAME."\"\n";
		&printAndDie("Valid sheet not found\n", $out_log, $log);
	}

	# Extracts titles of columns (descriping the content of that column)
	foreach my $col ($sheet -> {MinCol} ..  $sheet -> {MaxCol}) {
		foreach my $row ($sheet -> {MinRow} ..  $sheet -> {MaxRow}) {
   		my $cell = $sheet-> {Cells} [$row] [$col]; # only looking at the row containing titles for the column content (row 1)
			my $cell_val = $cell -> {Val};
			# Find headers
			if($row == 0){
				push(@headers, $cell_val);
				$header{$cell_val} = $col; # Creates a hash (key=title and value=column no.): the key is the description of the data type found in this column, and the value is the column number
			}
			# Values
			else{
				$input_meta[$row][$col] = $cell_val;
			}
		}
	}

	$min_col = $sheet -> {MinCol};
	$max_col = $sheet -> {MaxCol};
	$min_row = $sheet -> {MinRow}+1; # skip headers
	$max_row = $sheet -> {MaxRow};
}
# Tab separated format
else{
	open(TAB, "<$opt_input") or die("Couldn't open input file: $opt_input");
	my $header_line = <TAB>;
	@headers = split(/\t/, $header_line);

	my $col = 0;
	foreach my $header_title (@headers){
		$header{$header_title} = $col;
		$col++;
	}
	$max_col = $col--;

	my $row = 1;
	while(<TAB>){
    chomp;
		next if(not $_);
		$col = 0;
		my @values = split(/\t/);
		foreach my $val (@values){
			$input_meta[$row][$col] = $val;
			$col++;
		}
		$row++;
	}
	close(TAB);
  $row--;
	$max_row = $row;
}

#
# Output excel sheet
#
my $workbook = Excel::Writer::XLSX->new( $out_excel );
my $worksheet = $workbook->add_worksheet(SHEET_DATA_NAME);

# Creating formats: header, gmi, and error
my $format_header = $workbook->add_format();
$format_header->set_bold();

my $format_gmi = $workbook->add_format();
$format_gmi->set_color( 'green' );

my $cell_format_no_gmi = $workbook->add_format( bg_color => "grey" );

my $format_error = $workbook->add_format();
$format_error->set_bold();
$format_error->set_color( 'red' );
my $cell_format_error = $workbook->add_format( bg_color => "yellow" );

# Writing headers to spreadsheet.
for(my $i=0; $i<@headers; $i++){
	$worksheet->write( 0, $i, $headers[$i], $format_header );
}

#
# Verifying meta data
#

my $excel_row = 0;
my %files;
my $counter_row_errors = 0;
my $counter_row_mdm = 0;
my %mdm_incompatable;
my @metadata;
my %release_dates;
my $study_type;

# Go through the excel sheet extracting one row at a time (one sample) and extract the relevant information
my $rows_parsed = 0;
foreach my $row ($min_row .. $max_row) {
	$excel_row = $row;
#	my $excel_col = 0;

	# Initiate data hash at row index.
	my %tmp;
	$metadata[$excel_row] = \%tmp;
	my @attributes;

	my $is_row_mdm = 1;
	my $is_row_error = 0;
#	my $sample_i

	foreach my $header_title (@headers){
		my $is_error = 0;
		my $is_mdm = 1;

		my $cell_val = $input_meta[$row][$header{$header_title}];
#		my $cell_val = $sheet -> {Cells} [$row] [$header{$header_title}] -> {Val};

		# Checking missing values
		if(not $cell_val or $cell_val eq "-" or $cell_val eq "none" or $cell_val eq "missing"){
			if($required_headers{$header_title}){
				&logError($excel_row, $header_title, "Missing data in required field.");
				$is_error = 1;
				$is_row_error = 1;
			}
			elsif($mdm_req_headers{$header_title}){
				$is_mdm = 0;
				$is_row_mdm = 0;
				$mdm_incompatable{$excel_row} = 1;
			}
		}

		if($header_title eq SAMPLE_ID and defined($cell_val)){
			$metadata[$excel_row]->{SAMPLE_ID()} = $cell_val;
			push(@attributes, ENA_SAMPLE_ID);
		}
#		elsif($header eq GENUS){
#			$stored_genus = $cell_val;
#		}
		elsif($header_title eq ORGANISM and defined($cell_val)){
#			my $scientific_name = "$stored_genus $cell_val";
			if($excel_row == 1){
				print STDERR ("\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/ Please ignore this warning \\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\n");
				print STDERR ("\tDETAILS: The warning sometimes comes when initiating a new Bio::DB::Taxonomy object.\n");
			}
			my $db = Bio::DB::Taxonomy->new(-source => 'entrez');
			if($excel_row == 1){
				print STDERR ("/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\ Please ignore this warning /\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\\n");
			}
#			my $taxonid = $db->get_taxonid($scientific_name);
			my $taxonid = $db->get_taxonid($cell_val);
			if (not defined $taxonid){ #if no taxon id is found
				&logError($excel_row, $header_title, "Didn't find taxonid for $cell_val. Check Genus and Species, correct spelling?");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{ORGANISM()} = $cell_val;
			$metadata[$excel_row]->{taxon_id} = $taxonid;
		}
		elsif($header_title eq STRAIN_NAME and defined($cell_val)){
			unless($cell_val =~ /[A-Za-z0-9_]/){
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid strain name. Only letters (A-Z), numbers and underscores are valid.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{STRAIN_NAME()} = $cell_val;
			push(@attributes, ENA_STRAIN);
		}
		elsif($header_title eq HOST and defined($cell_val)){
			if(not $cell_val =~ /[A-Za-z0-9_]/){
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid host name. Only letters (A-Z), numbers and underscores are valid.");
				$is_error = 1;
				$is_row_error = 1;
			}

			# Translates host into genus species name, if not found in
			# translation file, the original value will remain.
			my $sci_host = $host_translation{lc($cell_val)};
			if(not defined($sci_host)){
				$sci_host = $cell_val;
			}

			# Check if taxonomy can be found
			my $db = Bio::DB::Taxonomy->new(-source => 'entrez');
			my $taxonid = $db->get_taxonid($sci_host);

			if (not defined $taxonid){ #if no taxon id is found
				&logError($excel_row, $header_title, "\"$cell_val\" is not a proper scientific host name, and is not found in the synonyms table. Please etiher add synonym OR fill in the scientific name of the host.");
				$is_error = 1;
				$is_row_error = 1;
			}
			# Set new cell value - only necessary if translation was used
			else{
				$cell_val = $sci_host;
			}

			$metadata[$excel_row]->{HOST()} = $cell_val;
			push(@attributes, ENA_HOST);
			push(@attributes, ENA_HOST_ASSOCIATED);
			push(@attributes, ENA_ENV_SAMPLE);
			$metadata[$excel_row]->{ENA_HOST_ASSOCIATED()} = "Yes";
			$metadata[$excel_row]->{ENA_ENV_SAMPLE()} = "No";
		}
		elsif($header_title eq PATHOGEN_TYPE and defined($cell_val)){
			unless( $cell_val eq "Clinical or host-associated pathogen" or $cell_val eq "environmental food or other pathogen" ){
				&logError($excel_row, $header_title, "\"$cell_val\" must match either \"Clinical or host-associated pathogen\" or \"environmental food or other pathogen\".");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{PATHOGEN_TYPE()} = $cell_val;
		}
		elsif($header_title eq HOST_DISEASE and defined($cell_val)){
			if(not $cell_val =~ /[A-Za-z0-9_]/){
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid disease input. Only letters (A-Z), numbers and underscores are valid.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{HOST_DISEASE()} = $cell_val;
			if($cell_val =~ /^healthy$/i or $cell_val =~ /^no$/i or $cell_val =~ /^none$/i or $cell_val eq "-"){
				$metadata[$excel_row]->{ENA_HOST_STATUS()} = "healthy";
				push(@attributes, ENA_HOST_STATUS);
			}
			elsif($cell_val){
				$metadata[$excel_row]->{ENA_HOST_STATUS()} = "diseased";
				push(@attributes, ENA_HOST_STATUS);
			}
		}
		elsif($header_title eq ISOLATION_SOURCE and defined($cell_val)){
			if(not $cell_val =~ /[A-Za-z0-9_]/){
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid isolation source input. Only letters, numbers and underscores are valid.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{ISOLATION_SOURCE()} = $cell_val;
			if( $metadata[$excel_row]->{ENA_HOST_ASSOCIATED()} eq "Yes" ){
				push(@attributes, ENA_ISOLATION_SOURCE_HOST);
			}
			elsif( $metadata[$excel_row]->{ENA_ENV_SAMPLE()} eq "Yes" ){
				push(@attributes, ENA_ISOLATION_SOURCE_NON_HOST);
			}
		}
		elsif($header_title eq STRAIN_SUPPLIER and defined($cell_val)){
			if(not $cell_val =~ /[A-Za-z0-9_@.]/ and $cell_val ne ""){
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid strain supplier input. Only letters, numbers, \@s, dots and underscores are valid.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{STRAIN_SUPPLIER()} = $cell_val;
			push(@attributes, ENA_STRAIN_SUPPLIER);
		}
		elsif($header_title eq ISOLATION_DATE and defined($cell_val)){
			my $ena_date;
			# Does cell_val start with 4 integers (store these), then 0 or 1 hyphen, then store everything after.
			if( $cell_val =~ /^(\d{4})\-{0,1}(.*)$/ ){
				my $year = $1;
				my $month_day = $2;
				my $date_error = "";

				$ena_date = $year;

				# Year cannot be higher than current year.
				if($year > $today[0]){
					$date_error .= "$year is not a valid year. ";
				}

				if($month_day and $month_day =~ /^(\d\d)\-{0,1}(.*)$/){
					my $month = $1;
					my $day = $2;
					my $ena_date = "$year-$month";
					if($month > 12 or $month < 1){
						$date_error .= "$month is not a valid month. ";
					}

					if($day and $day =~ /^(\d)(\d)$/){
						if($day > 31 or $day < 1){
							$date_error .= "$day is not a valid day.";
						}
						$ena_date = "$year-$month-$day";
					}
					elsif($day){
						$date_error .= "$cell_val is not a valid date string. It must conform to the format YYYY-MM-DD.";
					}
				}
				elsif($month_day){
					$date_error .= "$cell_val is not a valid date string. It must conform to the format YYYY-MM-DD.";
				}

				if($date_error){
					&logError($excel_row, $header_title, $date_error);
					$is_error = 1;
					$is_row_error = 1;
				}
			}
			else{
				&logError($excel_row, $header_title, "\"$cell_val\" is not a valid date. Dates must be formatted as: YYYY-MM-DD. Example: 2015-02-14");
				$is_error = 1;
				$is_row_error = 1;
			}

			unless($is_error){
				$metadata[$excel_row]->{ISOLATION_DATE()} = $cell_val;
				push(@attributes, ENA_ISOLATION_DATE);
				$metadata[$excel_row]->{ENA_ISOLATION_DATE()} = $ena_date;
			}
		}
		elsif($header_title eq COUNTRY and defined($cell_val)){
			$metadata[$excel_row]->{COUNTRY()} = $cell_val;
			push(@attributes, ENA_COUNTRY);
		}
		elsif($header_title eq AVAILABILITY and defined($cell_val)){
			unless($cell_val =~ /^delete$/i or $cell_val =~ /^public$/i){
				&logError($excel_row, $header_title, "\"$cell_val\" found where only the values \"delete\" and \"public\" are allowed.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{AVAILABILITY()} = $cell_val;
		}
		elsif($header_title eq DATA_RELEASE and defined($cell_val)){
			if( $cell_val =~ /^(\d{4})\-(\d{2})\-(\d{2})$/ ){
				my @rel_date = split(/\-/, $cell_val);
				my $diff_days = Delta_Days( @today, @rel_date );
				if($diff_days > 730){
					&logError($excel_row, $header_title, "\"$cell_val\" in release date cannot be more than 730 days (2 years) into the future. Diff was $diff_days.\n");
					$is_error = 1;
					$is_row_error = 1;
				}
			}
			else{
				&logError($excel_row, $header_title, "\"$cell_val\" Date not recognised. Format must be: YYYY-MM-DD.");
				$is_error = 1;
				$is_row_error = 1;
			}

			unless($is_error){
				$metadata[$excel_row]->{DATA_RELEASE()} = $cell_val;
				if( defined($release_dates{$cell_val}) ){
					push( @{$release_dates{$cell_val}}, $excel_row );
				}
				else{
					my @tmp = ( $excel_row );
					$release_dates{$cell_val} = \@tmp;
				}
			}
		}
		elsif($header_title eq SAMPLE_TYPE and defined($cell_val)){
			unless($cell_val =~ /^isolate$/i or $cell_val =~ /^metagenomic$/i){
				&logError($excel_row, $header_title, "\"$cell_val\" found where only the values \"isolate\" and \"metagenomic\" are allowed.");
				$is_error = 1;
				$is_row_error = 1;
			}
			elsif($cell_val =~ /^isolate$/i){
				if(defined($study_type) and $study_type eq "isolate"){
					$study_type = "isolate";
				}
				elsif(defined($study_type) and $study_type eq "metagenomic"){
					$study_type = "isolate_metagenomic";
				}
				else{
					$study_type = "isolate";
				}
				$cell_val = "isolate";
			}
			elsif($cell_val =~ /^metagenomic$/i){
				if(defined($study_type) and $study_type eq "metagenomic"){
					$study_type = "metagenomic";
				}
				elsif(defined($study_type) and $study_type eq "isolate"){
					$study_type = "isolate_metagenomic";
				}
				else{
					$study_type = "metagenomic";
				}
				$cell_val = "metagenomic";
			}
			$metadata[$excel_row]->{SAMPLE_TYPE()} = $cell_val;
		}
		elsif($header_title eq SEQ_METHOD and defined($cell_val)){
			unless($cell_val =~ /^LS454$/i or $cell_val =~ /^Illumina$/i or $cell_val =~ /^ABI SOLiD$/i or $cell_val =~ /^Ion Torrent$/i or $cell_val =~ /^unknown$/i){
				&logError($excel_row, $header_title, "\"$cell_val\" found where only the values \"LS454\", \"Illumina\", \"ABI SOLiD\", \"Ion Torrent\", and \"unknown\"");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{SEQ_METHOD()} = $cell_val;
		}
		elsif($header_title eq INSERT_SIZE and defined($cell_val)){
			unless($cell_val =~ /^\d+$/){
				&logError($excel_row, $header_title, "Insert Size:\"$cell_val\" seems to be invalid. It can only contain positive integers.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{INSERT_SIZE()} = $cell_val;
			# Assume paired
			$metadata[$excel_row]->{SEQ_TYPE()} = "paired";
		}
		elsif($header_title eq SEQ_TYPE and defined($cell_val) ){
			unless($cell_val =~ /^single$/i or $cell_val =~ /^paired$/i or $cell_val =~ /^mate-paired$/i or $cell_val =~ /^unknown$/i){
				&logError($excel_row, $header_title, "\"$cell_val\" found where only the values \"paired\", \"single\", \"mate-paired\", and \"unknown\" is allowed.");
				$is_error = 1;
				$is_row_error = 1;
			}
			$metadata[$excel_row]->{SEQ_TYPE()} = $cell_val;
		}
		elsif(($header_title eq LON or $header_title eq LAT) and defined($cell_val)){
			if(not $cell_val =~ /\d{2} \d{2}\.\d+/ and $cell_val){
				&logError($excel_row, $header_title);
				# print LOG ("\tThe Longitude/Latitude value \"$cell_val\" seems invalid. It must conform to the format: dd dd.ddd , where \"d\" is some integer (Ex: (55 47.244).\n");
				$is_error = 1;
				$is_row_error = 1;
			}
      if($cell_val){
        if($header_title eq LON){
  				$metadata[$excel_row]->{LON()} = $cell_val;
  				push(@attributes, ENA_LON);
  			}
  			else{
  				$metadata[$excel_row]->{LAT()} = $cell_val;
  				push(@attributes, ENA_LAT);
  			}
      }
		}
		elsif($header_title eq READ_DATA and defined($cell_val)){
			my @file_names = split(/\s+/, $cell_val);
			my @file_types;
			my @file_paths;
			foreach my $file_name (@file_names){
				# Check if file can be found
				my $file_path = $opt_data_folder."/$file_name";
				my($name, $dir, $suffix) = fileparse($file_name, qr/\.[^.]*/);
				# If uploaded this has to be the path in the dropbox!
				if($opt_uploaded){
					$file_path = "$name$suffix";
				}
				if($name =~ /__split__/){
					$suffix = ".fastq";
				}
				my $file_type;
				my($name_tmp, $dir_tmp, $suffix_2nd);
				if($suffix eq ".gz"){
					$suffix_2nd = ".gz";
					($name_tmp, $dir_tmp, $suffix) = fileparse($name, qr/\.[^.]*/);

				}
				unless(-s $file_path or $opt_uploaded ){
					&logError($excel_row, $header_title, "The file : \"$file_path\" could not be found. Please check that the file names has been written correctly.\n");
					$is_error = 1;
					$is_row_error = 1;
				}
				elsif($files{$file_path}){
					&logError($excel_row, $header_title, "A read file in this row was already associated with another isolate. Or maybe the same file name was entered for both read file 1 and 2.");
					$is_error = 1;
					$is_row_error = 1;
				}
				elsif($suffix ne ".fq" and $suffix ne ".fastq" and $suffix ne ".txt" and $suffix ne ".sff"){
					&logError($excel_row, $header_title, "'This tool only supports fastq and sff file formats. The detected format was: $suffix.\n");
					$is_error = 1;
					$is_row_error = 1;
				}
				else{
					if($suffix eq ".fq" or $suffix eq ".fastq" or $suffix eq ".txt"){
						$file_type = "fastq";
					}
					elsif($suffix eq ".sff"){
						$file_type = "sff";
					}
					push(@file_types, $file_type);
					push(@file_paths, $file_path);
					$files{$file_path} = $file_name;
				}
			}
			$metadata[$excel_row]->{read_types} = \@file_types;
			$metadata[$excel_row]->{READ_DATA()} = \@file_paths;
		}

		if($is_error){
			$worksheet->write( $excel_row, $header{$header_title}, $cell_val, $cell_format_error );
		}
		elsif(not $is_mdm){
			$worksheet->write( $excel_row, $header{$header_title}, $cell_val, $cell_format_no_gmi );
		}
		else{
			$worksheet->write( $excel_row, $header{$header_title}, $cell_val );
		}

#		$excel_col++;
	}
	$metadata[$excel_row]->{attributes} = \@attributes;

	# Rows containing errors are formatted bold red
	if($is_row_error){
		$worksheet->set_row( $excel_row, undef, $format_error );
		$counter_row_errors++;
	}
	# Rows that comply to GMI minimum data are formatted green
	elsif($is_row_mdm){
		$worksheet->set_row( $excel_row, undef, $format_gmi );
		$counter_row_mdm++;
	}

	$rows_parsed++;
}
$workbook->close();

# Prints log
my $log_printed = 1;
open(LOG, ">$out_log") or $log_printed = 0;
if($log_printed){
	print LOG ("$log");
	close(LOG);
}
else{
	print STDERR ("Couldn't print to log file: $opt_output.log\n");
	print STDERR ("Log will be printed to STDERR:\n");
	print STDERR ($log);
}

# Checks if any errors was found
if($counter_row_errors){
	print STDERR ("$counter_row_errors rows in the metadata spreadsheet contains errors.\n");
	print STDERR ("An overview of the metadata has been printed to the spreadsheet: $opt_output.xlsx\n");
	print STDERR ("Details on each error found is located in the log: $opt_output.log\n");
	print STDERR ("FAIL!\n");
	print STDERR ("DONE!\n");

	if($opt_web){
		my $downloadScript = "https://cge.cbs.dtu.dk/cge/download_files2.php";

		#open(OUT_WEB, ">>outputs/ENAUploader.out") or die("Couldn't write to out web file: $opt_web/outputs/ENAUploader.out\n");
		print STDOUT ("<br><br><h1><center>Errors found</center></h1><br><br>");
		print STDOUT ("<hr><br><br>\n");
		print STDOUT ("Meta data in the Sequence list was tested and errors were found in <b>$counter_row_errors rows</b><br>\n");
		print STDOUT ("Errors has been marked in a spreadsheet:<br>\n");
		print STDOUT ("<form action='$downloadScript' method='post'><input type='hidden' name='service' value='ENAUploader'><input type='hidden' name='version' value='2.1'><input type='hidden' name='filename' value='ENA_sub_output.xlsx'><input type='hidden' name='pathid' value='".$opt_web."'><input type='submit' value='Download Spreadsheet'></form></td>\n");
		print STDOUT ("<br><br>\n");
		print STDOUT ("Errors have been described in a log file:<br>\n");
		print STDOUT ("<form action='$downloadScript' method='post'><input type='hidden' name='service' value='ENAUploader'><input type='hidden' name='version' value='2.1'><input type='hidden' name='filename' value='ENA_sub_output.log'><input type='hidden' name='pathid' value='".$opt_web."'><input type='submit' value='Download Log'></form></td>\n");
		print STDOUT ("<br><br>\n");

		print STDOUT ("<center><h3>Log file:</h3></center><br>");

		print STDOUT ("<ul>\n");
		open(LOG, "<$out_log");
		my $line_counter = 1;
		while(<LOG>){
			chomp;
			# Even lines
			if($line_counter % 2 == 0){
				print STDOUT ("<li style=\"list-style-type:none\"><ul><li>$_</li></ul></li><br>\n");
			}
			# Uneven lines
			else{
				print STDOUT ("<li>$_</li>\n");
			}
			$line_counter++;
		}
		print STDOUT ("</ul>\n");

		print STDOUT ("<br><br><hr>\n");
		#close(OUT_WEB);
	}
	exit;
}
else{
	print STDERR ("No errors found.\n");
	print STDERR ("Rows containing minimum GMI metadata: $counter_row_mdm\n");

	if($opt_web){
		my $downloadScript = "https://cge.cbs.dtu.dk/cge/download_files2.php";
		#open(OUT_WEB, ">>outputs/ENAUploader.out") or die("Couldn't write to out web file: $opt_web/outputs/ENAUploader.out\n");

		print STDOUT ("<br><br><h1><center>Metadata parsed succesfully</center></h1><br><br>");
		print STDOUT ("<br><br>\n");
		print STDOUT ("$rows_parsed rows was parsed.\n");
		print STDOUT ("No errors was found in the metadata. <b>$counter_row_mdm rows</b> contained the minimum data requiremnts for GMI.<br>\n");
		print STDOUT ("The metadata can be reviewed in the spreadsheet:<br>\n");
		print STDOUT ("<form action='$downloadScript' method='post'><input type='hidden' name='service' value='ENAUploader'><input type='hidden' name='version' value='2.1'><input type='hidden' name='filename' value='ENA_sub_output.xlsx'><input type='hidden' name='pathid' value='".$opt_web."'><input type='submit' value='Download Spreadsheet'></form></td>\n");
		print STDOUT ("<br><br>\n");
		print STDOUT ("The files that will/has be(en) submitted to ENA:<br>\n");
		print STDOUT ("<form action='$downloadScript' method='post'><input type='hidden' name='service' value='ENAUploader'><input type='hidden' name='version' value='2.1'><input type='hidden' name='filename' value='xml_files.tar.gz'><input type='hidden' name='pathid' value='".$opt_web."'><input type='submit' value='Download XML files'></form></td>\n");
		print STDOUT ("<br><br>\n");
		#close(OUT_WEB);
	}
}

#
# Upload data to ENA dropbox
#
# TODO: Upload to individual folders?
if(not $opt_uploaded){
	my $upload_log = "";

	# Establish connection
	# Max 5 attempts, will wait 10 min between each attempt.
	my $connect_established = 0;
	my $connect_attempts = 0;
	my $ftp;
	while(not $connect_established or $connect_attempts < 6){
		$connect_attempts++;
		if( $ftp = Net::FTP -> new($hostname, Passive => 1, Debug => 1) ){
			$connect_established = 1;
		}
		else{
			$upload_log .= "Failed conncetion attempt $connect_attempts\n";
			sleep(600);
		}
	}


	if( $connect_established ){

		# Login in
		my $is_failed = 0;
		$ftp -> login ($username, $password) or $is_failed = 1;
		if($is_failed){
			$upload_log .= "Login failed.\n";
			&printAndDie("Failed to log in to ENAs FTP server.\n", $out_upload_log, $upload_log);
		}

		$ftp -> binary;


		# Excel rows start at index 1.
		my $row_counter = 1;
		foreach my $row_no (@metadata){
			# Index zero is empty
			next unless ($row_no);
# DEBUG
			print STDERR ("REF: $row_no\n");
			my @read_files = @{ $row_no->{READ_DATA()} };
			my $read_file_counter = 0;
			foreach my $file (@read_files){

				my $transfer_finished = 0;
				my($local_name, $local_dir, $local_suffix) = fileparse($file, qr/\.[^.]*/);

				my $dropbox = "$local_name$local_suffix"; # Used if file already exists on server.
				if( $ftp->size($dropbox) ){
					$upload_log .= ("Found, skipped upload: $local_name");
					$transfer_finished = 1;
				}

				my $transfer_attempts = 0;
				while( not $transfer_finished and $transfer_attempts < 6 ){
					$transfer_attempts++;
					$transfer_finished = 1;
					$dropbox = $ftp -> put($file) or $transfer_finished--;
					sleep(10) unless( $transfer_finished );
				}
				unless($transfer_finished){
					$upload_log .= "Could not upload file: $file\n";
					&printAndDie("FTP error: Could not upload file: $file\n",$out_upload_log, $upload_log);
				}

				my $checksum = &md5($file);
				$md5_sums{$dropbox} = $checksum;

				$read_files[$read_file_counter] = $dropbox; # this ref changes $file!!!
				$read_file_counter++;
			}

			# Save location in dropbox
			$metadata[$row_counter]->{READ_DATA()} = \@read_files;
#			$files{$file} = $dropbox;
			$row_counter++;
		}

		$ftp -> quit;
	}
	else{
		$upload_log .= "Giving up attempt to connect.\n";
		&printAndDie("Failed connect after $connect_attempts attempts\n", $out_upload_log, $upload_log);
	}

	my $log_printed = 1;
	open(LOG, ">$out_upload_log") or $log_printed = 0;
	if($log_printed){
		print LOG ("$upload_log");
		close(LOG);
	}
	else{
		print STDERR ("Couldn't print to log file: $out_upload_log\n");
		print STDERR ("Log will be printed to STDERR:\n");
		print STDERR ($upload_log);
	}
}

#
# Creating ENA XML meta data files
#

# Looping through the different release dates (since each release date has to be handled as a submission)
my $ena_run_prefix = "run_";
my $ena_exp_prefix = "exp_";
my $ena_sub_prefix = "submission_";
#my $counter_submissions = 0;
my $counter_uploaded = 0;
foreach my $date (keys %release_dates){
#	$counter_submissions++;
	print STDERR ("Creating XML files for submission date: $date\n");
	#
	# initiate the XML files
	#

	# initiate the submission XML
	my $xsi = "http://www.w3.org/2001/XMLSchema-instance";;
	my $doc_sub = XML::LibXML::Document -> new('1.0', 'UTF-8');
	my $root_sub = $doc_sub-> createElement('SUBMISSION_SET'); # the name of the root
	$root_sub -> addChild($doc_sub -> createAttribute('xmlns:xsi' => $xsi)); # add attribute to the root
	$root_sub -> addChild($doc_sub -> createAttribute('xsi:noNamespaceSchemaLocation' => "ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/SRA.submission.xsd"));
	$doc_sub -> setDocumentElement($root_sub); # set the root element of the document

	# Initiate study XML which due to new procedures at EBI is now project XMLs
	my $doc_project = XML::LibXML::Document -> new('1.0', 'UTF-8');
	my $root_project = $doc_project -> createElement('PROJECT_SET'); # the name of the root
	$root_project -> addChild($doc_project -> createAttribute('xmlns:xsi' => $xsi)); # add attribute to the root
	$root_project -> addChild($doc_project -> createAttribute('xsi:noNamespaceSchemaLocation' => "ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/SRA.sample.xsd"));
	$doc_project -> setDocumentElement($root_project); # set the root element of the document

	# Initiate sample XML
	my $doc_sample = XML::LibXML::Document -> new('1.0', 'UTF-8');
	my $root_sample = $doc_sample-> createElement('SAMPLE_SET'); # the name of the root
	$root_sample -> addChild($doc_sample -> createAttribute("xmlns:xsi" => $xsi)); # add attribute to the root
	$root_sample -> addChild($doc_sample -> createAttribute('xsi:noNamespaceSchemaLocation' => "ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/SRA.sample.xsd"));
	$doc_sample -> setDocumentElement($root_sample); # set the root element of the document

	# Initiate experiment XML
	my $doc_exp = XML::LibXML::Document -> new('1.0', 'UTF-8');
	my $root_exp = $doc_exp-> createElement('EXPERIMENT_SET'); # the name of the root
	$root_exp -> addChild($doc_exp -> createAttribute("xmlns:xsi" => $xsi)); # add attribute to the root
	$root_exp -> addChild($doc_exp -> createAttribute('xsi:noNamespaceSchemaLocation' => "ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/SRA.experiment.xsd"));
	$doc_exp -> setDocumentElement($root_exp); # set the root element of the document

	# initiate run XML
	my $doc_run = XML::LibXML::Document -> new('1.0', 'UTF-8');
	my $root_run = $doc_run-> createElement('RUN_SET'); # the name of the root
	$root_run -> addChild($doc_run -> createAttribute("xmlns:xsi" => $xsi)); # add attribute to the root
	$root_run -> addChild($doc_run -> createAttribute('xsi:noNamespaceSchemaLocation' => "ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/SRA.run.xsd"));
	$doc_run -> setDocumentElement($root_run); # set the root element of the document


# <RELATED_PROJECT>
# <PARENT_PROJECT accession="PRJEB5337" />
# </RELATED_PROJECT>
# </RELATED_PROJECTS>

	#
	# Create the content of the project/study XML
	#
	# Study alias/id, must be unique to our submission account!
	my $study_id = "study_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$date";
	# Creates an element (project) within the root
    my $project = $doc_project -> createElement('PROJECT');
	$project -> addChild ($doc_project -> createAttribute('alias' => $study_id));
	$project -> addChild($doc_project -> createAttribute('center_name' => $centre_ac));
	$root_project -> addChild($project);

    # Adding Title to study
	my $title = $doc_project -> createElement('TITLE');
	$title -> addChild( $doc_project -> createTextNode($opt_title) );
	$project -> addChild($title);

    # Adding Abstract to study
	my $abstract_atr = $doc_project -> createElement('DESCRIPTION');
	$abstract_atr -> addChild($doc_project -> createTextNode($abstract) );
	$project -> addChild($abstract_atr);

    my $subm_prj = $doc_project -> createElement('SUBMISSION_PROJECT');
	$subm_prj -> addChild($doc_project -> createAttribute('material' => 'DNA'));
	$subm_prj -> addChild($doc_project -> createAttribute('selection' => 'genome'));
	if($study_type eq 'isolate'){ #if the study type is one from the controlled vocabulary found here: https://www.ebi.ac.uk/ena/about/sra_preparing_metadata#study
		$subm_prj -> addChild($doc_project -> createAttribute('scope' => 'single isolate'));
	}
	elsif($study_type eq 'metagenomic'){
		$subm_prj -> addChild($doc_project -> createAttribute('scope' => 'community'));
	}
	else{ # else create a new study type (this is the case if the study is a mixture of single isolates and metagenomes)
		$subm_prj -> addChild($doc_project -> createAttribute('scope' => 'other'));
#		$subm_prj -> addChild($doc_project -> createAttribute('new_study_type' => 'Mixed types community and isolate'));
	}
	$project -> addChild($subm_prj);

	my $seq_prj = $doc_project -> createElement('SEQUENCING_PROJECT');
	$subm_prj -> addChild($seq_prj);

	my $obj_seq = $doc_project -> createElement('OBJECTIVE');
	$obj_seq -> addChild($doc_project -> createTextNode('sequence'));
	$subm_prj -> addChild($obj_seq);

	my $obj_raw = $doc_project -> createElement('OBJECTIVE');
	$obj_raw -> addChild($doc_project -> createTextNode('raw sequence'));
	$subm_prj -> addChild($obj_raw);

	my $rel_projects = $doc_project -> createElement('RELATED_PROJECTS');
	$project -> addChild($rel_projects);
	my $rel_project = $doc_project -> createElement('RELATED_PROJECT');
	$rel_projects -> addChild($rel_project);
	my $parent = $doc_project -> createElement('PARENT_PROJECT');
	$parent -> addChild($doc_project -> createAttribute('accession' => 'PRJEB6071'));
	$rel_project -> addChild($parent);

	# Process each row of input sheet, now stored in a hash
	my @rows = @{$release_dates{$date}};
	foreach my $row_no (@rows){

		#
		# Create content of experiment XML
		#

		my $experiment = $doc_exp -> createElement('EXPERIMENT');
		# Experiment alias/id, must be unique to our submission account!
		my $row_sample_id = $metadata[$row_no]->{SAMPLE_ID()};
		my $exp_alias = "exp_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$row_sample_id";
		$experiment -> addChild ($doc_exp -> createAttribute('alias' => $exp_alias));
		$experiment -> addChild($doc_exp -> createAttribute('center_name' => $centre_ac));
		$root_exp -> addChild($experiment);

		# Linking Experiment to Study
		my $study_ref = $doc_exp -> createElement('STUDY_REF');
		$study_ref -> addChild($doc_exp -> createAttribute('refname' => $study_id)); #refer to the study xml
		$experiment -> addChild($study_ref);

		my $design = $doc_exp -> createElement('DESIGN');
		$experiment -> addChild($design);
		my $design_desc = $doc_exp -> createElement('DESIGN_DESCRIPTION');
		#$design_desc -> addChild($doc_exp -> createTextNode('Put in details about the goal and experiment!'));
		$design -> addChild($design_desc);
		# Linking Sample
		my $sample_alias = "sam_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$row_sample_id";
		my $sample_ref = $doc_exp -> createElement('SAMPLE_DESCRIPTOR');
		$sample_ref -> addChild($doc_exp -> createAttribute('refname' => $sample_alias));
		$design -> addChild($sample_ref);


		my $lib_descript = $doc_exp -> createElement('LIBRARY_DESCRIPTOR');
		my $lib_name = $doc_exp -> createElement('LIBRARY_NAME');
		$lib_name -> addChild($doc_exp -> createTextNode($row_sample_id));
		$lib_descript -> addChild($lib_name);

		my $lib_stra = $doc_exp -> createElement('LIBRARY_STRATEGY');
		$lib_stra -> addChild($doc_exp -> createTextNode($opt_lib_strat));
		$lib_descript -> addChild($lib_stra);

		my $lib_source_row = $metadata[$row_no]->{SAMPLE_TYPE()};
		if($lib_source_row eq "isolate"){
			$lib_source_row = "GENOMIC";
		}
		elsif($lib_source_row eq "metagenomic"){
			$lib_source_row = "METAGENOMIC";
		}
		my $lib_source = $doc_exp -> createElement('LIBRARY_SOURCE');
		$lib_source -> addChild($doc_exp -> createTextNode($lib_source_row));
		$lib_descript -> addChild($lib_source);

		my $lib_sel = $doc_exp -> createElement('LIBRARY_SELECTION');
		$lib_sel -> addChild($doc_exp -> createTextNode($opt_lib_select)); #library selections needs to be added
		$lib_descript -> addChild($lib_sel);

		my $lib_layout = $doc_exp -> createElement('LIBRARY_LAYOUT');
		$lib_descript -> addChild($lib_layout);
		my $lib_layout_row = $metadata[$row_no]->{SEQ_TYPE()};
		if($lib_layout_row =~ /single/i){
			my $single = $doc_exp -> createElement('SINGLE');
			$lib_layout -> addChild($single);
		}
		elsif($lib_layout_row =~ /paired/i or $lib_layout_row =~ /mate-paired/i){
			my $paired = $doc_exp -> createElement('PAIRED');
			if( defined($metadata[$row_no]->{INSERT_SIZE()}) ){
				$paired -> addChild($doc_exp -> createAttribute('NOMINAL_LENGTH' => ($metadata[$row_no]->{INSERT_SIZE()}) )); #add information about insert size
			}
#DEBUG DUMMY VALUE!!!!!
#			elsif($opt_test or $opt_validate){
#				$paired -> addChild($doc_exp -> createAttribute('NOMINAL_LENGTH' => ("300") )); #add information about insert size
#			}
			$lib_layout -> addChild($paired);
		}
		else{
			my $missing = $doc_exp -> createElement('missing');
			$lib_layout -> addChild($missing);
		}

		my $platform_row = $metadata[$row_no]->{SEQ_METHOD()};
		my $instrument_row = $metadata[$row_no]->{SEQ_MODEL()};
		$design -> addChild($lib_descript);
		my $platform = $doc_exp -> createElement('PLATFORM');
		$experiment -> addChild($platform);
		if($platform_row =~ /^Ion Torrent$/i){
			my $ion = $doc_exp -> createElement('ION_TORRENT');
			$platform -> addChild($ion);
			my $ins_model = $doc_exp -> createElement('INSTRUMENT_MODEL');
			if(defined($instrument_row)){
				$ins_model -> addChild($doc_exp -> createTextNode($instrument_row));
			}
			else{
				$ins_model -> addChild($doc_exp -> createTextNode("unspecified"));
			}
			$ion -> addChild($ins_model);
		}
		elsif($platform_row =~ /^LS454$/i){
			my $ls454 = $doc_exp -> createElement('LS454');
			$platform -> addChild($ls454);
			my $ins_model = $doc_exp -> createElement('INSTRUMENT_MODEL');
			if(defined($instrument_row)){
				$ins_model -> addChild($doc_exp -> createTextNode($instrument_row));
			}
			else{
				$ins_model -> addChild($doc_exp -> createTextNode("unspecified"));
			}
			$ls454 -> addChild($ins_model);
		}
		elsif($platform_row =~ /^Illumina$/i){
			my $illumina = $doc_exp -> createElement('ILLUMINA');
			$platform -> addChild($illumina);
			my $ins_model = $doc_exp -> createElement('INSTRUMENT_MODEL');
			if(defined($instrument_row)){
				$ins_model -> addChild($doc_exp -> createTextNode($instrument_row));
			}
			else{
				$ins_model -> addChild($doc_exp -> createTextNode("unspecified"));
			}
			$illumina -> addChild($ins_model);
		}
		elsif($platform_row =~ /^ABI SOLiD$/i){
			my $abi = $doc_exp -> createElement('ABI_SOLID');
			$platform -> addChild($abi);
			my $ins_model = $doc_exp -> createElement('INSTRUMENT_MODEL');
			if(defined($instrument_row)){
				$ins_model -> addChild($doc_exp -> createTextNode($instrument_row));
			}
			else{
				$ins_model -> addChild($doc_exp -> createTextNode("unspecified"));
			}
			$abi -> addChild($ins_model);
		}
		elsif($platform_row =~ /^unknown$/i){
			# TODO: Exclude or "unspecified".
			die("At the moment we don't nkow how to handle this.\n");
		}

		#
		# Create content of run XML
		#
		my @files_row;
		my @tmp = @{ $metadata[$row_no]->{READ_DATA()} };
		if($tmp[0] =~ /(.*__split__.*)/){
			open(READS, "<$opt_data_folder/$1") or die("Couldn't open split file: $opt_data_folder/$1\n");
			while(<READS>){
				my @lines;
				my @entries = split(/\t+/);
				foreach my $entry (@entries){
					next if $entry =~ /__split__/;
					push(@lines, $entry);
				}
				push(@files_row, \@lines);
			}
		}
		else{
			my @lines = @{ $metadata[$row_no]->{READ_DATA()} };
			push(@files_row, \@lines);
		}

		my $run_counter = 0;

		foreach my $file_array_ref (@files_row){
			$run_counter++;
			my @file_array = @{$file_array_ref};

			my $run_alias = "run_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$row_sample_id"."_$run_counter";
			my $run = $doc_run -> createElement('RUN');
			$run -> addChild($doc_run -> createAttribute('alias' => $run_alias)); #create alias
			$run -> addChild($doc_run -> createAttribute('center_name' => $centre_ac));
			$root_run -> addChild($run);
			my $exp_ref = $doc_run -> createElement('EXPERIMENT_REF');
			$exp_ref -> addChild($doc_run -> createAttribute('refname' => $exp_alias)); #refer to the experiment xml
			$run -> addChild($exp_ref);
			my $data_block = $doc_run -> createElement('DATA_BLOCK');
			$run -> addChild($data_block);
			my $files = $doc_run -> createElement('FILES');
			$data_block -> addChild($files);

			my @file_types = @{ $metadata[$row_no]->{read_types} };
			# Claclulate MD5 could be paralized to speed up script!
			my $checksum1;
			my $checksum2;
	#		if($opt_test){
	#			# dummy values, to make test runs go faster
	#			$checksum1 = "e441bcca3d24923b885ba607b23c15e9";
	#			$checksum2 = "e441bcca3d24923b885ba607b23c15e9";
	#		}
	#		else{
				$checksum1 = $md5_sums{$file_array[0]};
	#		}
			### TODO: Check files and pairs? Is more than 2 accepted? ###
			die("Too many data files, only 1 or 2 accepted.") if(@file_array > 2);
			my $file = $doc_run -> createElement('FILE'); #refer to read_1 file
			$file -> addChild($doc_run -> createAttribute('filename' => $file_array[0]  )); #filename in drop box
			$file -> addChild($doc_run -> createAttribute('filetype' => $file_types[0] )); #extracted file ending
			$file -> addChild($doc_run -> createAttribute('checksum_method' => 'MD5'));
			$file -> addChild($doc_run -> createAttribute('checksum' => $checksum1));
			$files -> addChild($file);
			if($metadata[$row_no]->{SEQ_TYPE()} eq 'paired'){
				my $file_2 = $doc_run -> createElement('FILE');
				$file_2 -> addChild($doc_run -> createAttribute('filename' => $file_array[1]  )); #filename in drop box
				$file_2 -> addChild($doc_run -> createAttribute('filetype' => $file_types[1] )); #extracted file ending
				$file_2 -> addChild($doc_run -> createAttribute('checksum_method' => 'MD5'));
	#			unless($opt_test){
					$checksum2 = $md5_sums{$file_array[1]};
	#			}
				$file_2 -> addChild($doc_run -> createAttribute('checksum' => $checksum2 ));
				$files -> addChild($file_2);
			}
		}

		#
		# Create content of sample XML
		#
		my $sample = $doc_sample -> createElement('SAMPLE');
		$sample -> addChild ($doc_sample -> createAttribute('alias' => "sam_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$row_sample_id")); #create sample alias
		$sample -> addChild($doc_sample -> createAttribute('center_name' => $centre_ac));
		$sample -> addChild($doc_sample -> createAttribute('broker_name' => $centre_ac));
		$root_sample -> addChild($sample);
		my $sample_name = $doc_sample -> createElement('SAMPLE_NAME');
		$sample -> addChild($sample_name);
		my $taxon_id = $doc_sample -> createElement('TAXON_ID');
		$taxon_id -> addChild($doc_sample -> createTextNode( $metadata[$row_no]->{taxon_id} )); # add taxon ID
		$sample_name -> addChild($taxon_id);
		my $scientific = $doc_sample -> createElement('SCIENTIFIC_NAME');
		$scientific -> addChild($doc_sample -> createTextNode( $metadata[$row_no]->{ORGANISM()} )); # add scientific name
		$sample_name -> addChild($scientific);

		my $sample_atts = $doc_sample -> createElement('SAMPLE_ATTRIBUTES');
		$sample -> addChild($sample_atts);

		# Extract meta data hash for current sample.
		my @attributes = @{ $metadata[$row_no]->{attributes} };
		foreach my $ena_tag ( @attributes ){
#			if($opt_test){
#				next if( $ena_tag eq  ENA_LAT or $ena_tag eq ENA_LON );
#			}
			my $sample_att = $doc_sample -> createElement('SAMPLE_ATTRIBUTE');
			$sample_atts -> addChild($sample_att);
			my $sample_tag = $doc_sample -> createElement('TAG');
			$sample_tag -> addChild($doc_sample -> createTextNode($ena_tag)); # Specifies the type of information provided
			$sample_att -> addChild($sample_tag);
			my $sample_value = $doc_sample -> createElement('VALUE');
			my $meta_tag = $ena2meta{$ena_tag};
			my $val;
			if(defined($meta_tag)){
				$val = $metadata[$row_no]->{$meta_tag};
			}
			else{
				$val = $metadata[$row_no]->{$ena_tag};
			}
			$sample_value -> addChild($doc_sample -> createTextNode( $val )); # Add value
			$sample_att -> addChild($sample_value);
		}

		my @debug_array = keys(%mdm_incompatable);
		print ("Uploaded isolate: $row_sample_id\n");
		$counter_uploaded++;
#DEBUG
		if( not $mdm_incompatable{$row_no} and ($opt_test) and 1 == 1){
			my $sample_att = $doc_sample -> createElement('SAMPLE_ATTRIBUTE');
			$sample_atts -> addChild($sample_att);
			my $sample_tag = $doc_sample -> createElement('TAG');
			$sample_tag -> addChild($doc_sample -> createTextNode( GMI_MDM_TAG )); #specifies the type of information provided
			$sample_att -> addChild($sample_tag);
			my $sample_value = $doc_sample -> createElement('VALUE');
			$sample_value -> addChild($doc_sample -> createTextNode( GMI_MDM_VAL() )); #add ENA GMI:MDM check tag
			$sample_att -> addChild($sample_value);
		}
		else{
			print STDERR ("Sample: $row_sample_id did not meet the minimum requirements for GMI data.\n");
		}
#DEBUG
		if($opt_test and 0==1){
			my $sample_att1 = $doc_sample -> createElement('SAMPLE_ATTRIBUTE');
			$sample_atts -> addChild($sample_att1);
			my $sample_tag1 = $doc_sample -> createElement('TAG');
			$sample_tag1 -> addChild($doc_sample -> createTextNode("geographic location (latitude)"));
			$sample_att1 -> addChild($sample_tag1);
			my $sample_value1 = $doc_sample -> createElement('VALUE');
			$sample_value1 -> addChild($doc_sample -> createTextNode( "52.20" ));
			$sample_att1 -> addChild($sample_value1);
			my $sample_units1 = $doc_sample -> createElement('UNITS');
			$sample_units1 -> addChild($doc_sample -> createTextNode( "DD" ));
			$sample_att1 -> addChild($sample_units1);

			my $sample_att2 = $doc_sample -> createElement('SAMPLE_ATTRIBUTE');
			$sample_atts -> addChild($sample_att2);
			my $sample_tag2 = $doc_sample -> createElement('TAG');
			$sample_tag2 -> addChild($doc_sample -> createTextNode("geographic location (longitude)"));
			$sample_att2 -> addChild($sample_tag2);
			my $sample_value2 = $doc_sample -> createElement('VALUE');
			$sample_value2 -> addChild($doc_sample -> createTextNode( "0.12" ));
			$sample_att2 -> addChild($sample_value2);
			my $sample_units2 = $doc_sample -> createElement('UNITS');
			$sample_units2 -> addChild($doc_sample -> createTextNode( "DD" ));
			$sample_att2 -> addChild($sample_units2);
		}
		print STDERR "Succesfull generation of xml files for $row_sample_id\n";
	}

	# Create the filenames for the five XML files in the submission using the release date
	my $experiment_xml = "$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_experiment_$date.xml";
	my $study_xml      = "$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_study_$date.xml";
	my $run_xml        = "$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_run_$date.xml";
	my $sample_xml     = "$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_sample_$date.xml";
	my $submission_xml = "$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_submission_$date.xml";

	my $local_experiment_xml = $experiment_xml;
	my $local_study_xml = $study_xml;
	my $local_run_xml = $run_xml;
	my $local_sample_xml = $sample_xml;
	my $local_submission_xml = $submission_xml;

	if($opt_web){
		$local_experiment_xml = "downloads/".$experiment_xml;
		$local_study_xml = "downloads/".$study_xml;
		$local_run_xml = "downloads/".$run_xml;
		$local_sample_xml = "downloads/".$sample_xml;
		$local_submission_xml = "downloads/".$submission_xml;
	}

	# Printning study XML
	open(my $out_study, ">$local_study_xml") or die("Couldn't write study xml: $local_study_xml");
	binmode $out_study;
	$doc_project->toFH($out_study, 1); # If $format is 1, libxml2 will add ignorable white spaces, so the nodes content is easier to read. Existing text nodes will not be altered
	# Printning experiment XML
	open(my $out_exp, ">$local_experiment_xml") or die("Couldn't write experiment xml: $local_experiment_xml");
	binmode $out_exp;
	$doc_exp->toFH($out_exp, 1);
	# Printning sample XML
	open(my $out_sample, ">$local_sample_xml") or die("Couldn't write sample xml: $local_sample_xml");
	binmode $out_sample;
	$doc_sample->toFH($out_sample, 1);
	# Printning run XML
	open(my $out_run, ">$local_run_xml") or die("Couldn't write sample xml: $local_run_xml");
	binmode $out_run;
	$doc_run->toFH($out_run, 1);

	#
	# Create submission XML
	#
	my $submission = $doc_sub -> createElement('SUBMISSION');
	$submission -> addChild ($doc_sub -> createAttribute('alias' => "sub_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_rel$date"));
	$submission -> addChild($doc_sub -> createAttribute('center_name' => $centre_ac));
	$submission -> addChild($doc_sub -> createAttribute('broker_name' => $centre_ac));
	$root_sub -> addChild($submission); # Within the root

	my $actions = $doc_sub -> createElement('ACTIONS');
	$submission -> addChild($actions);

	my $action = $doc_sub -> createElement('ACTION');
	$actions -> addChild($action);
	my $add_study;
	if($opt_validate){
		$add_study = $doc_sub -> createElement('VALIDATE');
	}
	else{
		$add_study = $doc_sub -> createElement('ADD');
	}
	$add_study -> addChild($doc_sub -> createAttribute('source' => $study_xml)); #add the study XML to the submission
	$add_study -> addChild($doc_sub -> createAttribute('schema' => 'project'));
	$action -> addChild($add_study);

	$action = $doc_sub -> createElement('ACTION');
	$actions -> addChild($action);
	my $add_sample;
	if($opt_validate){
		$add_sample = $doc_sub -> createElement('VALIDATE');
	}
	else{
		$add_sample = $doc_sub -> createElement('ADD');
	}
	$add_sample -> addChild($doc_sub -> createAttribute('source' => $sample_xml)); #add the sample XML to the submission
	$add_sample -> addChild($doc_sub -> createAttribute('schema' => 'sample'));
	$action -> addChild($add_sample);

	$action = $doc_sub -> createElement('ACTION');
	$actions -> addChild($action);
	my $add_exp;
	if($opt_validate){
		$add_exp = $doc_sub -> createElement('VALIDATE');
	}
	else{
		$add_exp = $doc_sub -> createElement('ADD');
	}
	$add_exp -> addChild($doc_sub -> createAttribute('source' => $experiment_xml)); #add the experiment XML to the submission
	$add_exp -> addChild($doc_sub -> createAttribute('schema' => 'experiment'));
	$action -> addChild($add_exp);

	$action = $doc_sub -> createElement('ACTION');
	$actions -> addChild($action);
	my $add_run;
	if($opt_validate){
		$add_run = $doc_sub -> createElement('VALIDATE');
	}
	else{
		$add_run = $doc_sub -> createElement('ADD');
	}
	$add_run -> addChild($doc_sub -> createAttribute('source' => $run_xml)); #add the run XML to the submission
	$add_run -> addChild($doc_sub -> createAttribute('schema' => 'run'));
	$action -> addChild($add_run);

	# Add release date information
	$action = $doc_sub -> createElement('ACTION');
	$actions -> addChild($action);
	if($date eq 'Release'){
		my $release = $doc_sub -> createElement('RELEASE');
		$action -> addChild($release);
	}
	else{
		my $release = $doc_sub -> createElement('HOLD');
		$release -> addChild($doc_sub -> createAttribute('HoldUntilDate' => $date));
		$action -> addChild($release);
	}

	# Print submission XML
	open(my $out_sub, ">$local_submission_xml") or die("Couldn't write sample xml: $local_submission_xml");
	binmode $out_sub;
	$doc_sub->toFH($out_sub, 1);


	#
	# Upload XML files to ENA webpage using cURL
	#
	my $response = "";
#	if($opt_test and 1==0){
		print STDERR ("/usr/bin/curl --insecure -F SUBMISSION=@\"$local_submission_xml\" -F PROJECT=@\"$local_study_xml\" -F SAMPLE=@\"$local_sample_xml\" -F RUN=@\"$local_run_xml\" -F EXPERIMENT=@\"$local_experiment_xml\" \"$url\"\n");
#		open(my $response_fh, 'curl --insecure -F SUBMISSION=@"$local_submission_xml" -F STUDY=@"$local_study_xml" -F SAMPLE=@"$local_sample_xml" -F RUN=@"$local_run_xml" -F EXPERIMENT=@"$local_experiment_xml" "$url"' );
		open(my $response_fh1, "/usr/bin/curl --insecure -F SUBMISSION=@\"$local_submission_xml\" -F PROJECT=@\"$local_study_xml\" -F SAMPLE=@\"$local_sample_xml\" -F RUN=@\"$local_run_xml\" -F EXPERIMENT=@\"$local_experiment_xml\" \"$url\" |" );
		while(<$response_fh1>){
			chomp;
			$_ =~ s/^\s+//;
			$response .= $_;
		}
		close($response_fh1);
	}
#    else{
#        print STDERR ("DONE!\n");
#        exit;
#    }

	print STDERR ("Response from ENA:\n$response\n");

	if(not $response and not -s "downloads/ena_response.xml"){
		print STDERR ("No response from ENA.\n");
		exit;
	}
	elsif(-s "downloads/ena_response.xml"){
		open(DEBUG_ENA, "<downloads/ena_response.xml") or die;
		while(<DEBUG_ENA>){
			$response .= $_;
		}
		close(DEBUG_ENA);
	}

	# Process xml response from ENA
	my $parser = XML::LibXML->new();
	my $doc = XML::LibXML->load_xml( string => $response );

	# Print original XML
	# It's recieved as one line of text and therefore the parsed object is used
	# to make it prettier.
	my $response_xml_path = "ena_response.xml";
	my $response_html_path = "ena_response.html";
	if($opt_web){
		$response_xml_path = "downloads/ena_response.xml";
		$response_html_path = "downloads/ena_response.html";
	}
	open(my $ena_xml_fh, ">$response_xml_path") or warn("Couldn't write ena response xml file to: $response_xml_path\n");
	print {$ena_xml_fh} $doc->toStringHTML;
	close($ena_xml_fh);

	my %dist;
	my %ena_acc;
	open(my $response_fh, ">$response_html_path") or die("couldn't write to file: $response_html_path\n");
	my $sub_success = &xml_proc_node( $doc->getDocumentElement, \%dist, $response_fh, \%ena_acc, $date );

	print $response_fh ("<br><br>\n");
	print $response_fh ("<form action='https://cge.cbs.dtu.dk/cge/download_files2.php' method='post'><input type='hidden' name='service' value='ENAUploader'><input type='hidden' name='version' value='2.0'><input type='hidden' name='filename' value='ena_response.xml'><input type='hidden' name='pathid' value='".$opt_web."'><input type='submit' value='Download response (XML)'></form>");
	print $response_fh ("<br><br>\n");

    my $cge_pipeline_vars = "<!-- Variables for the CGE pipeline\n";

	if($sub_success and not $opt_test and not $opt_validate){
		print $response_fh ("<h2><center>Accession numbers</center></h2><br>\n");

		print $response_fh ("<b>Project/Study accession number:</b><br>");

		my $prj_acc = $ena_acc{project}->{"study_$time[0]$time[1]$time[2]_$today[0]$today[1]$today[2]_$date"};
		$cge_pipeline_vars .= "#study_acc=$prj_acc\n";
		print $response_fh ("$prj_acc<br><br>\n");

        print $response_fh ("<b>Submission accession number: </b><br>\n");
        my $subm_acc = $ena_acc{submission};
        $cge_pipeline_vars .= "#submission_acc=$subm_acc\n";
        print $response_fh ("$subm_acc<br><br>\n");

        my @aliases = keys( %{ $ena_acc{sample} } );
		print $response_fh ("<b>Sample accession numbers:</b><br>");
		print $response_fh ("<table cellpadding=\"20\">\n");
		foreach my $alias ( @aliases ){
			my $accession = $ena_acc{sample}->{$alias};
			print $response_fh ("<tr>\n");
			print $response_fh ("    <td>$alias</td>\n");
			print $response_fh ("    <td>$accession</td>\n");
			print $response_fh ("</tr>\n");
            $cge_pipeline_vars .= "#sample_acc=$accession\n";
		}
		print $response_fh ("</table><br><br>\n");

        @aliases = keys( %{ $ena_acc{run} } );
		print $response_fh ("<b>Run accession numbers:</b><br>");
		print $response_fh ("<table cellpadding=\"20\">\n");
		foreach my $alias ( @aliases ){
			my $accession = $ena_acc{run}->{$alias};
			print $response_fh ("<tr>\n");
			print $response_fh ("    <td>$alias</td>\n");
			print $response_fh ("    <td>$accession</td>\n");
			print $response_fh ("</tr>\n");
            $cge_pipeline_vars .= "#run_acc=$accession\n";
		}
		print $response_fh ("</table><br><br>\n");

        @aliases = keys( %{ $ena_acc{experiment} } );
		print $response_fh ("<b>Experiment accession numbers:</b><br>");
		print $response_fh ("<table cellpadding=\"20\">\n");
		foreach my $alias ( @aliases ){
			my $accession = $ena_acc{experiment}->{$alias};
			print $response_fh ("<tr>\n");
			print $response_fh ("    <td>$alias</td>\n");
			print $response_fh ("    <td>$accession</td>\n");
			print $response_fh ("</tr>\n");
            $cge_pipeline_vars .= "#exp_acc=$accession\n";
		}
		print $response_fh ("</table><br><br>\n");
	}
    elsif($opt_validate and $sub_success){
        $cge_pipeline_vars .= "#study_acc=Succes\n";
        $cge_pipeline_vars .= "#sample_acc=Succes\n";
        $cge_pipeline_vars .= "#run_acc=Succes\n";
        $cge_pipeline_vars .= "#exp_acc=Succes\n";
    }

    $cge_pipeline_vars .= "-->\n";
    print $response_fh ($cge_pipeline_vars);

	close($response_fh);

	if(-s $response_html_path and $opt_web){
		open(my $response_out_fh, "$response_html_path") or die("couldn't write to file: $response_html_path\n");
		while(<$response_out_fh>){
			print($_);
		}
	}
}


if($opt_web){
	system("/bin/tar -czf downloads/xml_files.tar.gz downloads/*.xml");
}

print STDERR ("DONE!\n");


exit;



#
# Functions
#

sub printAndDie{
	my ($error_msg, $log_file, $log2print) = @_;
	my $log_printed = 1;
	open(LOG, ">$log_file") or $log_printed = 0;
	if($log_printed){
		print LOG ("$log2print");
		close(LOG);
	}
	else{
		print STDERR ("Couldn't print to log file: $log_file\n");
		print STDERR ("Log will be printed to STDERR:\n");
		print STDERR ($log2print);
	}
	print STDERR ("DONE!\n");
	die("$error_msg");
}

sub logError{
	my ($excel_row, $header,$msg) = @_;
	$log .= "!!! Row: $excel_row Column: $header\n";
	$log .= "!!! \t$msg\n";
}

sub logMDM{
	my ($excel_row, $header,$msg) = @_;
	$log .= "### Row: $excel_row Column: $header\n";
	$log .= "### \t$msg\n";
}

sub loadTranslTable{
# ARGUMENT: Path to text file.
# Text file must be formatted as one translation pr line.
# Each line must consist of:
# some text 1 <tab> some text 2
# "some text 1" will be key and "some text 2" will become value.
	my $file = $_[0];
	my %out_table;
	open(TXT, "<$file") or die("Couldn't open text file for translation table.\n\tText file: $file\n");
	while(<TXT>){
		chomp;
		next if(/^#/);
		my( $key, $val ) = split(/\t+/);
		$out_table{$key} = $val;
	}
	return \%out_table;
}

# Calculate checksum md5 value for a given file with its file path
sub md5{
	my ($file) = @_; #input file path to the file of interest
	my $md5 = Digest::MD5->new;
	$md5->addpath($file);
	return $md5->hexdigest;
}

# process an XML tree node: if it's an element, update the
# distribution list and process all its children
sub xml_proc_node {
	my( $node, $dist, $fh, $ena_acc_ref, $success, $rel_date ) = @_;
	#my %ena_acc = %{$ena_acc_ref};

	return unless( $node->nodeType eq &XML_ELEMENT_NODE );
	$dist->{ $node->nodeName } ++;

	if($node->nodeName eq "ERROR"){
		my $content = $node->textContent;
		$content =~ s/^\s+//;
		print $fh ("<span style=\"color:#C00\"><b>Error:</b></span> $content<br>\n");
	}
	elsif($node->nodeName eq "RECEIPT"){
		my @attributelist = $node->attributes();
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;

			if($key eq "success"){
				if($val eq "false"){
					$success = 0;
					print $fh ("<h1><center><span style=\"color:#C00\">Submission failed</span></center></h1><br><br>\n");
				}
				else{
					$success = 1;
					print $fh ("<h1><center>Submission success</center></h1><br><br>\n");
				}

				print $fh ("<b>Response from ENA:</b><br><br>");
			}
			elsif($key eq "submissionFile"){
				print $fh ("<b>Submission file:</b> $val<br>\n");
			}
			elsif($key eq "receiptDate"){
				print $fh ("<b>ENA receipt date:</b> $val<br>\n");
			}
		}
	}
	elsif($node->nodeName eq "EXPERIMENT" and $success){
		my @attributelist = $node->attributes();
		my ($alias, $accession);
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;
			if($key eq "alias"){
				print $fh ("Restrictions on $val: ");
				$alias = $val;
			}
			elsif($key eq "status"){
				print $fh ("$val\n");
			}
			elsif($key eq "accession"){
				$accession = $val;
			}
		}
		if($accession and $alias){
			$$ena_acc_ref{experiment}->{$alias} = $accession;
		}
	}
	elsif($node->nodeName eq "INFO"){
		my $content = $node->textContent;
		$content =~ s/^\s+//;
		print $fh ("<b>Info:</b> $content<br>\n");
	}
	elsif($node->nodeName eq "RUN" and $success){
		my @attributelist = $node->attributes();
		my ($alias, $accession);
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;
			if($key eq "alias"){
				$alias = $val;
			}
			elsif($key eq "accession"){
				$accession = $val;
			}
		}
		if($accession and $alias){
			$$ena_acc_ref{run}->{$alias} = $accession;
		}
	}
	elsif($node->nodeName eq "SAMPLE" and $success){
		my @attributelist = $node->attributes();
		my ($alias, $accession);
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;
			if($key eq "alias"){
				$alias = $val;
			}
			elsif($key eq "accession"){
				$accession = $val;
			}
		}
		if($accession and $alias){
			$$ena_acc_ref{sample}->{$alias} = $accession;
		}
	}
	elsif($node->nodeName eq "STUDY" and $success){
		my @attributelist = $node->attributes();
		my ($alias, $accession);
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;
			if($key eq "alias"){
				$alias = $val;
			}
			elsif($key eq "accession"){
				$accession = $val;
			}
		}
		if($accession and $alias){
			$$ena_acc_ref{project}->{$alias} = $accession;
		}
	}
	elsif($node->nodeName eq "SUBMISSION" and $success){
		my @attributelist = $node->attributes();
		my $accession;
		foreach(@attributelist){
			my($key, $val) = split(/=/);
			$val =~ s/\"//g;
			$key =~ s/\s+//g;
			if($key eq "accession"){
				$accession = $val;
			}
		}
		if($accession){
			$$ena_acc_ref{submission} = $accession;
		}
	}

	foreach my $child ( $node->getChildnodes ) {
    	&xml_proc_node( $child, $dist, $fh, $ena_acc_ref, $success );
	}

	return $success;
}
